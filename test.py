import time
from random import randint
import __init__ as wmfstat # analogue to import wmfstat

if __name__ == '__main__':

    bar = wmfstat.StatusBar('default', interval=0.02) 

    #bar['text'] = wmfstat.elements.Text(text = 'foo bar', color='#af0', position='right')
    #bar['rect'] = wmfstat.elements.Rect(position='right', dimensions=(100,12), color='#af0')
    #bar['image'] = wmfstat.elements.Image(position='right', dimensions=(0,0), imagepath='/home/phryk/bar.png')
    #bar['progress'] = wmfstat.elements.Pgbar(position='right', dimensions=(200,8), border=2, value=50, valuemax=100, bgcolor='#8c0', fgcolor='#af0')
    #bar['position'] = wmfstat.elements.Posbar(position='right', dimensions=(200,8), cursor=5, value=50, valuemax=100, bgcolor='#8c0', fgcolor='#af0')
    #bar['graph'] = wmfstat.elements.Graph(position='right', dimensions=(200,12), value=10, valuemax=100, bgcolor='#8c0', fgcolor='#af0', name='foo bar')
    bar['rider'] = wmfstat.elements.Knightrider(position='right', dimensions=(200,8), cursor=5, value=50, valuemax=100, bgcolor='#a80000', fgcolor='#d40000')

    bar['graph'] = wmfstat.elements.Cpugraph(position='right', dimensions=(200,12), value=0, valuemax=100, bgcolor='#8c0', fgcolor='#af0', name='cpu')

    bar['graph0'] = wmfstat.elements.Cpugraph(position='left', dimensions=(50, 9), value=0, valuemax=100, bgcolor='#8c0', fgcolor='#af0', name='core0', cpu=0)

    bar['graph1'] = wmfstat.elements.Cpugraph(position=(100, 0), dimensions=(50, 9), value=0, valuemax=100, bgcolor='#8c0', fgcolor='#af0', name='core1', cpu=1)

    bar['graph2'] = wmfstat.elements.Memgraph(position=(200, 0), dimensions=(50, 9), value=0, valuemax=100, bgcolor='#8c0', fgcolor='#af0', name='mem')

    bar.start()

    while True:
        time.sleep(100)
