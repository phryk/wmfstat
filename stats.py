from threading import Thread
from time import sleep
import psutil


class SingletonMeta(type):

    """Singleton Metaclass"""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMeta, cls).__call__(*args, **kwargs)

        return cls._instances[cls]



class Superthread(Thread):
   
    __metaclass__ = SingletonMeta

    daemon = True # make this a daemon thread

    values = None
    interval = None
    stopped = None

    def __init__(self, interval = 1.0):

        super(self.__class__, self).__init__()

        self.stopped = True
        self.values = {}
        self.threads = {}
        self.interval = interval

    def __getitem__(self, name):

        try:
            return self.values[name]

        except: raise


    def __setitem__(self, name, value):

        try:
            self.values[name] = value

        except: raise


    def run(self):

        self.stopped = False


        while not self.stopped:

            self.update()
            sleep(self.interval)


    def stop(self):

        self.stopped = True


    def update(self):

        for val in self.values.itervalues():

            try:

                thr = Thread(target=val)
                thr.daemon = True # make this a daemon thread
                thr.start()
            
            except: raise



class Stat(object):

    """Parent class for all elements displaying stats"""

    __metaclass__ = SingletonMeta

    values = None


    def __init__(self):

        self.values = {}
        self.update() #fill values with right keys

        sthread = Superthread()
        sthread[self.__class__.__name__] = self


    def __getitem__(self, name):

        try:
            return self.values[name]

        except: raise

    
    def __setitem(self, name, value):

        try:
            self.values[name] = value

        except: raise


    def __call__(self):
        self.update()


    def keys(self):
        return self.values.keys()


    def update(self):
        
        """This method to be overriden by child classes"""

        pass



class Cpu(Stat):

    def update(self):

        self.values['all'] = psutil.cpu_percent()

        i = 0
        for val in psutil.cpu_percent(percpu=True):
            self.values[i] = val
            i+=1


class Mem(Stat):

    def update(self):

        vmem = psutil.virtual_memory()
        self.values['total'] = vmem.total
        self.values['used'] = vmem.used
        self.values['free'] = vmem.free
        self.values['percent'] = vmem.percent

