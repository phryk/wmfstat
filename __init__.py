#!/usr/bin/env python
from collections import OrderedDict
from threading import Thread
from time import sleep
from stats import Superthread
import elements


class Renderthread(Thread):

    daemon = True

    stopped = None
    sbar = None

    def __init__(self, sbar, interval = 1.0):

        super(self.__class__, self).__init__()

        self.stopped = True
        self.interval = interval
        self.sbar = sbar


    def run(self):

        self.stopped = False

        while not self.stopped:
        
            self.sbar.update()
            self.sbar.render()

            sleep(self.interval)


    def stop(self):
        self.stopped = True


# statusbar wrapper class

class StatusBar(object):
    
    name = None
    items = None
    fifo = None
    sthread = None
    rthread = None


    def __init__(self, name, items = None, fifo = None, sthread = None, interval = 1.0):

        self.name = name

        if items:
            self.items = OrderedDict(items)
        
        else:
            self.items = OrderedDict()


        if fifo:
            self.fifo = fifo

        else:
            self.fifo = open('/tmp/wmfs-:0.fifo', 'w')


        self.sthread = Superthread(interval)
        self.rthread = Renderthread(self, interval)


    def __getitem__(self, name):

        try:
            return self.items[name]
        except: raise


    def __setitem__(self, name, value):
        self.items[name] = value


    def __str__(self):

        s = self.name + ' '

        for item in self.items.itervalues():
            s += str(item)
        
        return s


    def keys(self, newkeys = None):

        if newkeys:

            try:
                self.items.setkeys(newkeys)

            except: raise

        return self.items.keys()


    def update(self):

        for item in self.items.itervalues():

            if hasattr(item, 'update'):
                item.update()


    def render(self):

        self.fifo.write('status '+str(self)+"\n")
        self.fifo.close()
        self.fifo = open(self.fifo.name, 'w')

    def start(self):

        self.sthread.start()
        self.rthread.start()
