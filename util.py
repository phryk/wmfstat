class Position(object):

    x = None
    y = None
    special = None

    def __init__(self, pos):
        
        if type(pos).__name__ == 'str':
            self.special = pos
        else:
            self.x = pos[0]
            self.y = pos[1]
    

    def __str__(self):

        if self.special:
            return self.special
        else:
            return str(self.x) +';'+ str(self.y)



#color class, mostly for future use to hold functions for saturation, etc
class Color(object):

    color = None

    def __init__(self, color):
        self.color = color


    def __str__(self):
        return self.color



class Dimensions(object):

    width = None
    height = None

    def __init__(self, x, height = None):
        
        if not height:
            self.width = x[0]
            self.height = x[1]

        else:
            self.width = x
            self.height = height

    
    def __str__(self):
        return str(self.width) +';'+ str(self.height)
