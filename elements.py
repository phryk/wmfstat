from util import Color, Position, Dimensions
import stats

#TODO: decide on either __setattr__ or __setitem__ for classes


class Status(object):

    """Base Status class"""

    def __init__(self, **kwargs):

        for key in self.params:
            if kwargs.has_key(key):

                value = kwargs[key]

                if key in self.mk.keys():
                    value = self.mk[key](self, value)

                setattr(self, key, value)

            else:
                raise AttributeError(self.__class__.__name__+': Key not found in kwargs: '+ key+'; found:'+ str(kwargs.keys())) #ugly


    def __str__(self):

        l = []

        for pkey in self.params:
            l.append(str(getattr(self, pkey)))

        s = self.ini + '['
        s += ';'.join(l)
        s += ']'

        return s
    

    def mkpos(self, pos):
        if type(pos).__name__ != 'Position':
            pos = Position(pos)

        return pos


    def mkcol(self, col):
        if type(col).__name__ != 'Color':
            col = Color(col)

        return col

    
    def mkdim(self, dim):
        if type(dim).__name__ != 'Dimensions':
            dim = Dimensions(dim)

        return dim


    params = []

    mk = {
        'position': mkpos,
        'color': mkcol,
        'dimensions': mkdim
    }

    ini = ''



# WMFS base items below this

class Text(Status):

    ini = '^s'
    params = ['position', 'color', 'text']



class Rect(Status):
    
    ini = '^R'
    params = ['position', 'dimensions', 'color']



class Image(Status):

    ini = '^i'
    params = ['position', 'dimensions', 'imagepath']



class Pgbar(Status):

    ini = '^p'
    params = ['position', 'dimensions', 'border', 'value', 'valuemax', 'bgcolor', 'fgcolor']



class Posbar(Status):

    ini = '^P'
    params = ['position', 'dimensions', 'cursor', 'value', 'valuemax', 'bgcolor', 'fgcolor']



class Graph(Status):

    ini = '^g'
    params = ['position', 'dimensions', 'value', 'valuemax', 'bgcolor', 'fgcolor', 'name']



# Advanced classes below this

class Cpugraph(Graph):

    cpu = None
    data = None

    def __init__(self, **kwargs):
        
        self.data = stats.Cpu()

        if kwargs.has_key('cpu'):

            self.cpu = kwargs['cpu']
            del kwargs['cpu']

        else:

            self.cpu = 'all'    

        super(self.__class__, self).__init__(**kwargs)


    def update(self):

        load = self.data[self.cpu]
        self.value = int(load * (self.valuemax / 100))


class Memgraph(Graph):

    data = None

    def __init__(self, **kwargs):

        self.data = stats.Mem()

        super(self.__class__, self).__init__(**kwargs)
    

    def update(self):

        load = self.data['percent']
        self.value = int(load * (self.valuemax / 100))



class Knightrider(Posbar):

    """The awesomest element of all"""

    direction = None


    def __init__(self, **kwargs):


      self.ini = super(Knightrider, self).ini
      self.params = super(Knightrider, self).params

      self.direction = 'l'

      super(self.__class__, self).__init__(**kwargs)
      


    def update(self):

        if self.direction == 'l' and self.value == 0: 
            self.direction = 'r'

        elif self.direction == 'r' and self.value == self.valuemax:
            self.direction = 'l'


        if self.direction == 'l':
            self.value -= 1

        else:
            self.value += 1
